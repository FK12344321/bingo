FROM debian  

WORKDIR /opt/bingo 
COPY bingo config.yaml ./ 

RUN adduser bingo 

RUN mkdir -p /opt/bongo/logs/96fe29e677 && \
    touch /opt/bongo/logs/96fe29e677/main.log && \
    chmod 777 /opt/bongo/logs/96fe29e677/main.log

USER bingo 

EXPOSE 13254

ENTRYPOINT ./bingo prepare_db && ./bingo run_server